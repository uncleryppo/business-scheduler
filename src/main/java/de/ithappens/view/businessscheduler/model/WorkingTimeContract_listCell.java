package de.ithappens.view.businessscheduler.model;

import de.ithappens.businessscheduler.model.WorkingTimeContract;

/** 
 * <p>Title: org.y3.businessscheduler.view.model - WorkingTimeContract_listCell</p>
 * <p>Description: </p>
 * <p>Copyright: 2016</p>
 * <p>Organisation: IT-Happens.de</p>
 * @author Christian.Rybotycky
*/
public class WorkingTimeContract_listCell extends IModel_listCell<WorkingTimeContract>{

    @Override
    public String getItemText(WorkingTimeContract item) {
        return item != null ? I_node.convertToDate(item.getValidFrom()) : null;
    }

}
