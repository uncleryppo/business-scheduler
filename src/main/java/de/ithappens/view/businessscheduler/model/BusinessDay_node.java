package de.ithappens.view.businessscheduler.model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTimePicker;
import de.ithappens.businessscheduler.model.BusinessDay;
import de.ithappens.businessscheduler.model.ModelController;
import de.ithappens.businessscheduler.model.WorkingTimeContract;
import de.ithappens.businessscheduler.view.IconDictionary;
import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.GaugeBuilder;
import eu.hansolo.medusa.Marker;
import eu.hansolo.medusa.Section;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 * Copyright: 2016 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class BusinessDay_node extends IModel_node<BusinessDay> {
	
	private Logger log = LogManager.getLogger();

    private JFXTimePicker startTime_tp, endTime_tp;
    private JFXCheckBox enableScrollToSelectedBusinessDay_cbx,
            enableTimeStampAnimation_cbx;
    private JFXDatePicker day_dp;
    private JFXButton today_btn, homeoffice_btn,
            startHourPlus_btn, startHourMinus_btn, endHourPlus_btn, endHourMinus_btn,
            startMinutePlus_btn, startMinuteMinus_btn, endMinutePlus_btn, endMinuteMinus_btn;
    private JFXTextArea notes_txa;
    private Gauge dayStatusBrutto_gauge, dayStatusNetto_gauge, dayCompletion_gauge;
    private final double dayStatusMaxValue = 12.0;
    private Label calendarWeek_lbl, today_lbl;
    private Marker contractTimeWithoutBreakBrutto_marker, contractTimeWithoutBreakNetto_marker,
            contractTimeWithBreakBrutto_marker, contractTimeWithBreakNetto_marker;
    private Section contractTimeMaxAllowedWithBreakBrutto_section, contractTimeMaxAllowedWithBreakNetto_section,
            contractTimeWithoutBreakToWithBreakBrutto_section, contractTimeWithoutBreakToWithBreakNetto_section;

    public BusinessDay_node(ModelController mc) {
        super(mc);
    }

    private JFXButton createFXButton(ImageView icon, String tooltipText, EventHandler<ActionEvent> setOnAction) {
        JFXButton createdButton = new JFXButton();
        icon.setFitHeight(40d);
        icon.setFitWidth(40d);
        createdButton.setGraphic(icon);
        createdButton.setTooltip(new Tooltip(tooltipText));
        createdButton.setOnAction(setOnAction);
        return createdButton;
    }

    @Override
    protected void buildUi() {
        //preferences
        enableTimeStampAnimation_cbx = new JFXCheckBox(MC().RB().ENABLE_TIMESTAMP_ANIMATION);
        enableTimeStampAnimation_cbx.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {
                    MC().PREFS().setEnableTimestampAnimation(new_val);
                });
        enableScrollToSelectedBusinessDay_cbx = new JFXCheckBox(MC().RB().ENABLE_SCROLL_TO_SELECTED_BUSINESS_DAY);
        enableScrollToSelectedBusinessDay_cbx.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {
                    MC().PREFS().setEnableScrollToSelectedBusinessDay(new_val);
                    setEnableScrollToSelectedModel(new_val);
                });
        //form
        Label day_lbl = new Label(MC().RB().BUSINESS_DAY);
        day_dp = new JFXDatePicker();
        day_dp.setShowWeekNumbers(true);
        day_dp.setOnAction((ActionEvent t) -> {
            updateModelFromUI();
        });
        today_btn = new JFXButton();
        ImageView today_imgv = IconDictionary.TODAY_IMAGE_VIEW;
        today_imgv.setFitHeight(40d);
        today_imgv.setFitWidth(40d);
        today_btn.setGraphic(today_imgv);
        today_btn.setOnAction((ActionEvent e) -> {
            day_dp.setValue(LocalDate.now());
            updateModelFromUI();
            updateCalculatedValuesView();
        });
        startHourPlus_btn = createFXButton(IconDictionary.HOUR_PLUS(),
                MC().RB().HOUR_PLUS,
                (ActionEvent e) -> {
                    startTime_tp.setValue(startTime_tp.getValue().plusHours(1));
                }
        );
        startHourMinus_btn = createFXButton(IconDictionary.HOUR_MINUS(),
                MC().RB().HOUR_MINUS,
                (ActionEvent e) -> {
                    startTime_tp.setValue(startTime_tp.getValue().minusHours(1));
                }
        );
        startMinutePlus_btn = createFXButton(IconDictionary.MINUTE_PLUS(),
                MC().RB().MINUTE_PLUS,
                (ActionEvent e) -> {
                    startTime_tp.setValue(startTime_tp.getValue().plusMinutes(1));
                }
        );
        startMinuteMinus_btn = createFXButton(IconDictionary.MINUTE_MINUS(),
                MC().RB().MINUTE_MINUS,
                (ActionEvent e) -> {
                    startTime_tp.setValue(startTime_tp.getValue().minusMinutes(1));
                }
        );
        homeoffice_btn = createFXButton(IconDictionary.HOMEOFFICE_IMAGE_VIEW,
                MC().RB().HOME_OFFICE,
                (ActionEvent e) -> {
                    day_dp.setValue(LocalDate.now());
                    startTime_tp.setValue(LocalTime.of(8, 0));
                    endTime_tp.setValue(LocalTime.of(14, 0));
                    notes_txa.setText(MC().RB().HOME_OFFICE);
                }
        );

        Label startTime_lbl = new Label(MC().RB().START);
        startTime_tp = new JFXTimePicker();
        startTime_tp.setIs24HourView(true);
        startTime_tp.setOnAction((ActionEvent t) -> {
            updateModelFromUI();
        });
        calendarWeek_lbl = new Label();
        calendarWeek_lbl.getStyleClass().add("calculatedInfoInForm");
        today_lbl = new Label();
        today_lbl.getStyleClass().add("calculatedTodayInfo");

        Label endTime_lbl = new Label(MC().RB().END);
        endTime_tp = new JFXTimePicker();
        endTime_tp.setIs24HourView(true);
        endTime_tp.setOnAction((ActionEvent t) -> {
            updateModelFromUI();
        });
        endHourPlus_btn = createFXButton(IconDictionary.HOUR_PLUS(),
                MC().RB().HOUR_PLUS,
                (ActionEvent e) -> {
                    endTime_tp.setValue(endTime_tp.getValue().plusHours(1));
                }
        );
        endHourMinus_btn = createFXButton(IconDictionary.HOUR_MINUS(),
                MC().RB().HOUR_MINUS,
                (ActionEvent e) -> {
                    endTime_tp.setValue(endTime_tp.getValue().minusHours(1));
                }
        );
        endMinutePlus_btn = createFXButton(IconDictionary.MINUTE_PLUS(),
                MC().RB().MINUTE_PLUS,
                (ActionEvent e) -> {
                    endTime_tp.setValue(endTime_tp.getValue().plusMinutes(1));
                }
        );
        endMinuteMinus_btn = createFXButton(IconDictionary.MINUTE_MINUS(),
                MC().RB().MINUTE_MINUS,
                (ActionEvent e) -> {
                    endTime_tp.setValue(endTime_tp.getValue().minusMinutes(1));
                }
        );

        Label notes_lbl = new Label(MC().RB().NOTES);
        notes_txa = new JFXTextArea();
        notes_txa.textProperty().addListener((obs, oldText, newText) -> {
            updateModelFromUI();
        });
        //status
        contractTimeWithoutBreakBrutto_marker = new Marker(0, MC().RB().ALLOWED_WORKING_MINUTES_PER_DAY_WITHOUT_BREAK, Color.BLUE);
        contractTimeWithoutBreakNetto_marker = new Marker(0, MC().RB().ALLOWED_WORKING_MINUTES_PER_DAY_WITHOUT_BREAK, Color.BLUE);
        contractTimeWithoutBreakToWithBreakBrutto_section = new Section(0, dayStatusMaxValue, Color.YELLOW);
        contractTimeWithoutBreakToWithBreakNetto_section = new Section(0, dayStatusMaxValue, Color.YELLOW);
        contractTimeWithBreakBrutto_marker = new Marker(0, MC().RB().WORKING_MINUTES_PER_DAY, Color.DARKGREEN);
        contractTimeWithBreakNetto_marker = new Marker(0, MC().RB().WORKING_MINUTES_PER_DAY, Color.DARKGREEN);
        contractTimeMaxAllowedWithBreakBrutto_section = new Section(0, dayStatusMaxValue, Color.INDIANRED);
        contractTimeMaxAllowedWithBreakNetto_section = new Section(0, dayStatusMaxValue, Color.INDIANRED);
        dayStatusBrutto_gauge = GaugeBuilder.create()
                .skinType(Gauge.SkinType.GAUGE).prefSize(200, 200)
                .maxValue(dayStatusMaxValue)
                .decimals(2)
                .unit(MC().RB().HOURS)
                .title(MC().RB().BRUTTO)
                .returnToZero(false) //setting true occurs value stays '0' after bind again. needed for animation
                .animated(true)
                .markersVisible(false)
                .markers(contractTimeWithBreakBrutto_marker, contractTimeWithoutBreakBrutto_marker)
                .sectionsVisible(true)
                .sections(contractTimeMaxAllowedWithBreakBrutto_section, contractTimeWithoutBreakToWithBreakBrutto_section)
                .build();
        dayStatusBrutto_gauge.animatedProperty().bind(enableTimeStampAnimation_cbx.selectedProperty());
        dayStatusNetto_gauge = GaugeBuilder.create()
                .skinType(Gauge.SkinType.GAUGE).prefSize(200, 200)
                .maxValue(dayStatusMaxValue)
                .decimals(2)
                .unit(MC().RB().HOURS)
                .title(MC().RB().NETTO)
                .returnToZero(false) //setting true occurs value stays '0' after bind again. needed for animation
                .animated(false)
                .markersVisible(false)
                .markers(contractTimeWithBreakNetto_marker, contractTimeWithoutBreakNetto_marker)
                .sectionsVisible(true)
                .sections(contractTimeMaxAllowedWithBreakNetto_section, contractTimeWithoutBreakToWithBreakNetto_section)
                .build();
        dayStatusNetto_gauge.animatedProperty().bind(enableTimeStampAnimation_cbx.selectedProperty());
        dayCompletion_gauge = GaugeBuilder.create()
                .skinType(Gauge.SkinType.BULLET_CHART).prefSize(50, 200)
                .maxValue(100)
                .decimals(0)
                .unit("%")
                .title(MC().RB().COMPLETED).valueVisible(true)
                .returnToZero(false) //setting true occurs value stays '0' after bind again. needed for animation
                .animated(true)
                .build();
        dayCompletion_gauge.animatedProperty().bind(enableTimeStampAnimation_cbx.selectedProperty());
        //layout
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(25);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(20);
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(55);
        getColumnConstraints().addAll(column1, column2, column3);
        int row = 0;
        int rowspan = 1;
        int colspan = 1;
        //illustration
        FlowPane buttonBar = getButtonBar();
        buttonBar.getChildren().add(enableTimeStampAnimation_cbx);
        buttonBar.getChildren().add(enableScrollToSelectedBusinessDay_cbx);
        add(buttonBar, 0, row, 4, rowspan);
        row++;
        add(getModelListView(), 0, row, colspan, 8);
        add(day_lbl, 1, row);
        add(new FlowPane(day_dp, today_btn, homeoffice_btn, calendarWeek_lbl, new Label(" "), today_lbl), 2, row);
        row++;
        add(startTime_lbl, 1, row);
        add(new FlowPane(startTime_tp, startHourPlus_btn, startHourMinus_btn, startMinutePlus_btn, startMinuteMinus_btn), 2, row);
        row++;
        add(endTime_lbl, 1, row);
        add(new FlowPane(endTime_tp, endHourPlus_btn, endHourMinus_btn, endMinutePlus_btn, endMinuteMinus_btn), 2, row);
        row++;
        add(notes_lbl, 1, row);
        add(notes_txa, 2, row, colspan, 2);
        row++;
        row++;
        add(new Separator(), 1, row, getColumnConstraints().size() - 1, rowspan);
        row++;
        add(dayStatusBrutto_gauge, 1, row);
        add(dayStatusNetto_gauge, 2, row++);
        add(dayCompletion_gauge, 1, row, colspan + 1, rowspan);
        //statistics update timer
        Timeline timeline = new Timeline(new KeyFrame(
                Duration.millis(1000),
                ae -> updateStatistics()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
        updateFormMode();
    }

    @Override
    public String getTitle() {
        return MC().RB().BUSINESS_DAYS;
    }

    @Override
    protected void actionBeforeSaveVault() {
    }

    @Override
    protected void actionAfterSaveVault() {
    }

    private void updateStatistics() {
        double durationBrutto = 0;
        double durationNetto = 0;
        double dayCompleteRate = 0;
        WorkingTimeContract validContract = null;
        boolean contractDetailsVisible = false;
        double allowedWorkingHoursPerDayWithoutBreak = 0;
        double allowedWorkingHoursPerDayWithBreakBrutto = 0;
        double allowedWorkingHoursPerDayWithBreakNetto = 0;
        double contractTimeMaxAllowedWithBreakStartNetto = 0;
        double contractTimeMaxAllowedWithBreakStartBrutto = 0;
        BusinessDay currentModel = getCurrentModel();
        if (currentModel != null && currentModel.getDate() != null) {
            durationNetto = durationBrutto = currentModel.getDuration();
            if (currentModel.getEndBusiness_timeStamp() != null) {
                Calendar current = Calendar.getInstance();
                double diffExpectedMinutesBrutto = getDiffMinutes(currentModel.getStartBusiness_timeStamp(), currentModel.getEndBusiness_timeStamp());
                double diffCurrentMinutes = getDiffMinutes(currentModel.getStartBusiness_timeStamp(), current);
                dayCompleteRate = diffCurrentMinutes / diffExpectedMinutesBrutto * 100;
                if (dayCompleteRate > 100) {
                    dayCompleteRate = 100;
                }
            }
            validContract = Utils.findValidContract(getCurrentVault().getWorkingTimecontracts(), currentModel.getDate());
            contractDetailsVisible = validContract != null;
            if (validContract != null) {
                allowedWorkingHoursPerDayWithoutBreak = validContract.getAllowedWorkingHoursPerDayWithoutBreak();
                allowedWorkingHoursPerDayWithBreakBrutto = validContract.getWorkingHoursPerDayWithBreak();
                allowedWorkingHoursPerDayWithBreakNetto = validContract.getWorkingHoursPerDayWithBreakNetto();
                contractTimeMaxAllowedWithBreakStartNetto = validContract.getMaxAllowedWorkingHoursPerDayNetto();
                contractTimeMaxAllowedWithBreakStartBrutto = validContract.getMaxAllowedWorkingHoursPerDay();
                durationNetto = validContract.getDurationNetto(durationBrutto);
            }
        }
        //status
        dayStatusBrutto_gauge.setValue(durationBrutto);
        dayStatusNetto_gauge.setValue(durationNetto);
        dayCompletion_gauge.setValue(dayCompleteRate);
        contractTimeWithoutBreakToWithBreakBrutto_section.setStart(allowedWorkingHoursPerDayWithoutBreak);
        contractTimeWithoutBreakToWithBreakBrutto_section.setStop(allowedWorkingHoursPerDayWithBreakBrutto);
        contractTimeWithoutBreakToWithBreakNetto_section.setStart(allowedWorkingHoursPerDayWithoutBreak);
        contractTimeWithoutBreakToWithBreakNetto_section.setStop(allowedWorkingHoursPerDayWithBreakNetto);
        contractTimeMaxAllowedWithBreakBrutto_section.setStart(contractTimeMaxAllowedWithBreakStartBrutto);
        contractTimeMaxAllowedWithBreakNetto_section.setStart(contractTimeMaxAllowedWithBreakStartNetto);
        contractTimeWithoutBreakBrutto_marker.setValue(allowedWorkingHoursPerDayWithoutBreak);
        contractTimeWithoutBreakNetto_marker.setValue(allowedWorkingHoursPerDayWithoutBreak);
        contractTimeWithBreakBrutto_marker.setValue(allowedWorkingHoursPerDayWithBreakBrutto);
        contractTimeWithBreakNetto_marker.setValue(allowedWorkingHoursPerDayWithBreakNetto);
        dayStatusBrutto_gauge.setMarkersVisible(contractDetailsVisible);
        dayStatusNetto_gauge.setMarkersVisible(contractDetailsVisible);
        dayStatusBrutto_gauge.setSectionsVisible(contractDetailsVisible);
        dayStatusNetto_gauge.setSectionsVisible(contractDetailsVisible);
    }

    @Override
    protected void bindToCurrentModel() {
        log.debug("bindToCurrentModel");
        LocalDate startDay = null;
        LocalTime startTime = null;
        LocalTime endTime = null;
        String notes = null;
        BusinessDay currentModel = getCurrentModel();
        if (currentModel != null) {
            if (currentModel.getStartBusiness_timeStamp() != null) {
                startDay = convertDate(currentModel.getStartBusiness_timeStamp());
                startTime = convertTime(currentModel.getStartBusiness_timeStamp());
            }
            if (currentModel.getEndBusiness_timeStamp() != null) {
                endTime = convertTime(currentModel.getEndBusiness_timeStamp());
            }
            notes = currentModel.getNotes_string();
        }
        notes_txa.setText(notes);
        day_dp.setValue(startDay);
        startTime_tp.setValue(startTime);
        endTime_tp.setValue(endTime);
        updateFormMode();
        updateStatistics();
    }

    private void updateFormMode() {
        //ui
        boolean modelAvailable = getCurrentModel() != null;
        day_dp.disableProperty().setValue(!modelAvailable);
        today_btn.disableProperty().setValue(!modelAvailable);
        startTime_tp.disableProperty().setValue(!modelAvailable);
        endTime_tp.disableProperty().setValue(!modelAvailable);
        notes_txa.disableProperty().setValue(!modelAvailable);
        updateCalculatedValuesView();
    }

    private void updateCalculatedValuesView() {
        BusinessDay currentModel = getCurrentModel();
        String cwString = "";
        String todayString = "";
        if (currentModel != null) {
            int cw = currentModel.calculateCalendarWeek();
            if (cw != 0) {
                cwString = MC().RB().CW + " " + currentModel.calculateCalendarWeek();
            }
            Calendar currentTimeStamp = Calendar.getInstance();
            if (currentModel.isToday()) {
                todayString = MC().RB().TODAY;
            }
        }
        calendarWeek_lbl.setText(cwString);
        today_lbl.setText(todayString);
    }

    public long getDiffMinutes(Calendar start, Calendar end) {
        if (start == null || end == null) {
            return 0l;
        }
        long diffMilis = end.getTimeInMillis() - start.getTimeInMillis();
        return TimeUnit.MILLISECONDS.toMinutes(diffMilis);
    }

    protected void updateModelFromUI() {
        if (getCurrentModel() != null) {
            try {
                if (day_dp.getValue() != null) {
                    if (startTime_tp.getValue() == null) {
                        startTime_tp.setValue(LocalTime.now());
                    }
                    if (endTime_tp.getValue() == null) {
                        endTime_tp.setValue(LocalTime.now());
                    }
                    Calendar startTimeStamp = convertDateTime(
                            day_dp.getValue(),
                            startTime_tp.getValue());
                    getCurrentModel().setStartBusiness_timeStamp(startTimeStamp);
                }
                if (startTime_tp.getValue() != null) {
                    Calendar endTimeStamp = convertDateTime(
                            day_dp.getValue(),
                            endTime_tp.getValue());
                    getCurrentModel().setEndBusiness_timeStamp(endTimeStamp);
                }
                getCurrentModel().setNotes_string(notes_txa.getText());
                getModelListView().refresh();
                updateCalculatedValuesView();
            } catch (Exception ex) {
                log.error(ex);
                ex.printStackTrace();
            }
        }
    }

    @Override
    protected IModel_listCell<BusinessDay> getModelCellFactory() {
        return new BusinessDay_listCell();
    }

    @Override
    protected List<BusinessDay> getModelsFromCurrentVault() {
        return MC().getCurrentVault() != null ? MC().getCurrentVault().getBusinessDays() : null;
    }

    @Override
    protected void setModelsToCurrentVault(List<BusinessDay> models) {
        if (MC().getCurrentVault() != null) {
            MC().getCurrentVault().setBusinessDays(models);
        }
    }

    @Override
    protected BusinessDay createModel() {
        return new BusinessDay();
    }

    @Override
    public void bindUi() {
        super.bindUi();
        enableTimeStampAnimation_cbx.setSelected(MC().PREFS().getEnableTimestampAnimation());
        enableScrollToSelectedBusinessDay_cbx.setSelected(MC().PREFS().getEnableScrollToSelectedBusinessDay());
        setEnableScrollToSelectedModel(enableScrollToSelectedBusinessDay_cbx.selectedProperty().getValue());
    }

}
