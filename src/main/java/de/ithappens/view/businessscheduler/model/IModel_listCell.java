package de.ithappens.view.businessscheduler.model;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;

/** 
 * Copyright: 2016 - 2018
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 * @param <T> item type
*/
public abstract class IModel_listCell<T> extends ListCell<T> {

        @Override
        public void updateItem(T item, boolean empty) {
            super.updateItem(item, empty);
            Label name_lbl = new Label();
            if (item != null) {
                name_lbl.setText(getItemText(item));
                getStyleClass().add(getStyleClass(item));
            }
            setGraphic(name_lbl);
        }

        public abstract String getItemText(T item);
        
        public String getStyleClass(T item) {
            return "";
        }

    }
