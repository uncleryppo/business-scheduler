package de.ithappens.view.businessscheduler.model;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import de.ithappens.businessscheduler.model.ModelController;
import de.ithappens.businessscheduler.model.WorkingTimeContract;
import java.time.LocalDate;
import java.util.List;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;

/** 
 * Copyright: 2016 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
*/
public class WorkingTimeContract_node extends IModel_node<WorkingTimeContract> {
    
    private JFXDatePicker validFrom_dp;
    private JFXTextField workingMinutesPerDay_tf,
            allowedWorkingMinutesPerDayWithoutBreak_tf,
            breakMinutesPerDay_tf,
            maxAllowedWorkingMinutesPerDay_tf;
    
    public WorkingTimeContract_node(ModelController mc) {
        super(mc);
    }

    @Override
    protected void actionBeforeSaveVault() {
    }

    @Override
    protected void actionAfterSaveVault() {
    }

    @Override
    protected void buildUi() {
        //form
        Label validFrom_lbl = new Label(MC().RB().VALID_FROM);
        validFrom_dp = new JFXDatePicker();
        validFrom_dp.setShowWeekNumbers(true);
        validFrom_dp.setOnAction((ActionEvent t) -> {
            if (getCurrentModel() != null) {
                getCurrentModel().setValidFrom(convertDate(validFrom_dp.getValue()));
                getModelListView().refresh();
            }
        });
        Label workingMinutesPerDay_lbl = new Label(MC().RB().WORKING_MINUTES_PER_DAY);
        workingMinutesPerDay_tf = new JFXTextField();
        workingMinutesPerDay_tf.textProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
            if (getCurrentModel() != null) {
                getCurrentModel().setWorkingMinutesPerDay(convertToInteger(t1));
                getModelListView().refresh();
            }
        });
        Label allowedWorkingMinutesPerDayWithoutBreak_lbl = new Label(MC().RB().ALLOWED_WORKING_MINUTES_PER_DAY_WITHOUT_BREAK);
        allowedWorkingMinutesPerDayWithoutBreak_tf = new JFXTextField();
        allowedWorkingMinutesPerDayWithoutBreak_tf.textProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
            if (getCurrentModel() != null) {
                getCurrentModel().setAllowedWorkingMinutesPerDayWithoutBreak(convertToInteger(t1));
                getModelListView().refresh();
            }
        });
        Label breakMinutesPerDay_lbl = new Label(MC().RB().BREAK_MINUTES_PER_DAY);
        breakMinutesPerDay_tf = new JFXTextField();
        breakMinutesPerDay_tf.textProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
            if (getCurrentModel() != null) {
                getCurrentModel().setBreakMinutesPerDay(convertToInteger(t1));
                getModelListView().refresh();
            }
        });
        Label maxAllowedWorkingMinutesPerDay_lbl = new Label(MC().RB().MAX_ALLOWED_WORKING_MINUTES_PER_DAY);
        maxAllowedWorkingMinutesPerDay_tf = new JFXTextField();
        maxAllowedWorkingMinutesPerDay_tf.textProperty().addListener((ObservableValue<? extends String> ov, String t, String t1) -> {
            if (getCurrentModel() != null) {
                getCurrentModel().setMaxAllowedWorkingMinutesPerDay(convertToInteger(t1));
                getModelListView().refresh();
            }
        });
        //layout
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(30);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(30);
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(35);
        getColumnConstraints().addAll(column1, column2, column3);
        int row = 0;
        int rowspan = 1;
        int colspan = 1;
        //illustration
        FlowPane buttonBar = getButtonBar();
        add(buttonBar, 0, row, 4, rowspan);
        row++;
        add(getModelListView(), 0, row, colspan, 5);
        add(validFrom_lbl, 1, row);
        add(validFrom_dp, 2, row);
        row++;
        add(workingMinutesPerDay_lbl, 1, row);
        add(workingMinutesPerDay_tf, 2, row);
        row++;
        add(allowedWorkingMinutesPerDayWithoutBreak_lbl, 1, row);
        add(allowedWorkingMinutesPerDayWithoutBreak_tf, 2, row);
        row++;
        add(breakMinutesPerDay_lbl, 1, row);
        add(breakMinutesPerDay_tf, 2, row);
        row++;
        add(maxAllowedWorkingMinutesPerDay_lbl, 1, row);
        add(maxAllowedWorkingMinutesPerDay_tf, 2, row);
        row++;
        add(new Separator(), 1, row, getColumnConstraints().size() - 1, rowspan);
    }

    @Override
    public String getTitle() {
        return MC().RB().WORKING_TIME_CONTRACTS;
    }

    @Override
    protected void bindToCurrentModel() {
        
        LocalDate validFrom = null;
        Integer workingMinutesPerDay = null;
        Integer allowedWorkingMinutesPerDayWithoutBreak = null;
        Integer breakMinutesPerDay = null;
        Integer maxAllowedWorkingMinutesPerDay = null;
        if (getCurrentModel() != null) {
            validFrom = convertDate(getCurrentModel().getValidFrom());
            workingMinutesPerDay = getCurrentModel().getWorkingMinutesPerDay();
            allowedWorkingMinutesPerDayWithoutBreak = getCurrentModel().getAllowedWorkingMinutesPerDayWithoutBreak();
            breakMinutesPerDay = getCurrentModel().getBreakMinutesPerDay();
            maxAllowedWorkingMinutesPerDay = getCurrentModel().getMaxAllowedWorkingMinutesPerDay();
        }
        validFrom_dp.setValue(validFrom);;
        workingMinutesPerDay_tf.setText(convert(workingMinutesPerDay));
        allowedWorkingMinutesPerDayWithoutBreak_tf.setText(convert(allowedWorkingMinutesPerDayWithoutBreak));
        breakMinutesPerDay_tf.setText(convert(breakMinutesPerDay));
        maxAllowedWorkingMinutesPerDay_tf.setText(convert(maxAllowedWorkingMinutesPerDay));
    }

    @Override
    protected IModel_listCell<WorkingTimeContract> getModelCellFactory() {
        return new WorkingTimeContract_listCell();
    }

    @Override
    protected List<WorkingTimeContract> getModelsFromCurrentVault() {
        return MC().getCurrentVault() != null ? MC().getCurrentVault().getWorkingTimecontracts() : null;
    }

    @Override
    protected void setModelsToCurrentVault(List<WorkingTimeContract> models) {
        if (MC().getCurrentVault() != null) {
            MC().getCurrentVault().setWorkingTimecontracts(models);
        }
    }

    @Override
    protected WorkingTimeContract createModel() {
        return new WorkingTimeContract();
    }

}
