package de.ithappens.view.businessscheduler.model;

import com.jfoenix.controls.JFXTextField;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import org.apache.commons.lang3.math.NumberUtils;
import de.ithappens.businessscheduler.model.ModelController;
import de.ithappens.businessscheduler.model.Vault;

/** 
 * Copyright: 2016 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
*/
public abstract class I_node extends GridPane {
    
    private final ModelController MC;
    
    public I_node(ModelController mc) {
        MC = mc;
        init();
        buildUi();
    }
    
    public ModelController MC() {
        return MC;
    }
    
    protected Vault getCurrentVault() {
        return vaultExists() ? MC().getCurrentVault() : null;
    }
    
    protected boolean vaultExists() {
        return MC() != null && MC().getCurrentVault() != null;
    }
    
    private void init() {
        int gap = 25;
        int ins = 30;
        setHgap(gap);
        setVgap(gap);
        setPadding(new Insets(ins, ins, ins, ins));
        setPrefSize(900, 600);
    }

    protected abstract void buildUi();
    
    public abstract void bindUi();
    
    public abstract String getTitle();
    
    public static String convertToDateAndTimeAndSeconds(Calendar cal) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        return convertCalendar(sdf, cal);
    }
    
    public static String convertToDateAndTime(Calendar cal) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return convertCalendar(sdf, cal);
    }
    
    public static String convertToDate(Calendar cal) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return convertCalendar(sdf, cal);
    }
    
    public static String convertCalendar(SimpleDateFormat sdf, Calendar cal) {
        if (cal != null) {
            return sdf.format(cal.getTime());
        } else {
            return null;
        }
    }
    
    public String convert(Integer integer) {
        if (integer != null) {
            return Integer.toString(integer);
        } else {
            return null;
        }
    }
    
    public Integer convertToInteger(String string) {
        if (NumberUtils.isNumber(string)) {
            return Integer.parseInt(string);
        } else {
            return null;
        }
    }
    
    public JFXTextField notEditableTextField() {
        JFXTextField tf = new JFXTextField();
        tf.setEditable(false);
        return tf;
    }
    
    public Calendar convertDate(LocalDate ld) {
        if (ld != null) {
            Calendar cal = Calendar.getInstance();
            cal.set(ld.getYear(), ld.getMonthValue() - 1, ld.getDayOfMonth());
            return cal;
        } else {
            return null;
        }
    }
    
    public LocalDate convertDate(Calendar cal) {
        if (cal != null) {
            return LocalDate.of(cal.get(Calendar.YEAR), 
                    cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
        } else {
            return null;
        }
    }
    
    public Calendar convertTime(LocalTime lt) {
        if (lt != null) {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, lt.getHour());
            cal.set(Calendar.MINUTE, lt.getMinute());
            return cal;
        } else {
            return null;
        }
    }
    
    public LocalTime convertTime(Calendar cal) {
        if (cal != null) {
            return LocalTime.of(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
        } else {
            return null;
        }
    }
    
    public Calendar convertDateTime(LocalDate ld, LocalTime lt) {
        Calendar timeStampFromLd = convertDate(ld);
        Calendar timeStampFromLt = convertTime(lt);
        Calendar convertedTimeStamp = Calendar.getInstance();
        convertedTimeStamp.set(
                timeStampFromLd != null ? timeStampFromLd.get(Calendar.YEAR) : 0,
                timeStampFromLd != null ? timeStampFromLd.get(Calendar.MONTH): 0,
                timeStampFromLd != null ? timeStampFromLd.get(Calendar.DAY_OF_MONTH) : 0,
                timeStampFromLt != null ? timeStampFromLt.get(Calendar.HOUR_OF_DAY) : 0,
                timeStampFromLt != null ? timeStampFromLt.get(Calendar.MINUTE) : 0);
        return convertedTimeStamp;
    }

}
