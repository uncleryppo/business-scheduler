package de.ithappens.view.businessscheduler.model;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextField;
import de.ithappens.businessscheduler.model.ModelController;
import de.ithappens.businessscheduler.view.IconDictionary;
import java.io.File;
import java.io.IOException;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.stage.FileChooser;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Copyright: 2016 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class Vault_node extends I_node {
	
	private Logger log = LogManager.getLogger();

    private Button loadVault_btn, saveVault_btn, newVault_btn;
    private JFXCheckBox enableBackup_cbx;
    private JFXTextField currentVaultLocation_txf,
            creationDate_txf, creationUser_txf, creationSystem_txf,
            changeDate_txf, changeUser_txf, changeSystem_txf;

    public Vault_node(ModelController mc) {
        super(mc);
    }

    public void loadLastUsedVaultFile(boolean enableBackup) throws IOException {
        String fileLocation = MC().PREFS().getLocationOfLastUsedVaultFile();
        if (StringUtils.isNotEmpty(fileLocation)) {
            File vaultFile = new File(fileLocation);
            MC().loadVault(vaultFile, enableBackup);
            bindUi(vaultFile);
        }
    }

    @Override
    protected void buildUi() {
        loadVault_btn = new Button(MC().RB().LOAD, new ImageView(IconDictionary.LOAD_ICON));
        loadVault_btn.getStyleClass().add("vault_load_btn");
        loadVault_btn.setOnAction((ActionEvent e) -> {
            try {
                actionLoadVault(MC().PREFS().getEnableBackup());
            } catch (IOException ex) {
                log.error(ex);
            }
        });
        saveVault_btn = new Button(MC().RB().SAVE, new ImageView(IconDictionary.SAVE_ICON));
        saveVault_btn.getStyleClass().add("vault_save_btn");
        saveVault_btn.setOnAction((ActionEvent e) -> {
            try {
                actionSaveVault();
            } catch (IOException ex) {
                log.error(ex);
            }
        });
        newVault_btn = new Button(MC().RB().NEW_VAULT, new ImageView(IconDictionary.NEW_VAULT_ICON));
        newVault_btn.getStyleClass().add("vault_new_btn");
        newVault_btn.setOnAction((ActionEvent e) -> {
            try {
                actionNewVault();
            } catch (IOException ex) {
                log.error(ex);
            }
        });
        Label enabledBackup_lbl = new Label(MC().RB().ENABLE_BACKUP);
        enableBackup_cbx = new JFXCheckBox();
        enableBackup_cbx.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {
                    MC().PREFS().setEnableBackup(new_val);
                });
        Label currentVaultLocation_lbl = new Label(MC().RB().VAULT_LOCATION);
        currentVaultLocation_txf = new JFXTextField();
        Label creationDate_lbl = new Label(MC().RB().CREATION_DATE);
        creationDate_txf = notEditableTextField();
        Label creationUser_lbl = new Label(MC().RB().CREATION_USER);
        creationUser_txf = notEditableTextField();
        Label creationSystem_lbl = new Label(MC().RB().CREATION_SYSTEM);
        creationSystem_txf = notEditableTextField();
        Label changeDate_lbl = new Label(MC().RB().CHANGE_DATE);
        changeDate_txf = notEditableTextField();
        Label changeUser_lbl = new Label(MC().RB().CHANGE_USER);
        changeUser_txf = notEditableTextField();
        Label changeSystem_lbl = new Label(MC().RB().CHANGE_SYSTEM);
        changeSystem_txf = notEditableTextField();
        //layout
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(40);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(40);
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(40);
        getColumnConstraints().addAll(column1, column2, column3);
        int row = 0;
        int rowspan = 1;
        int colspan = 1;
        add(loadVault_btn, 0, row);
        add(saveVault_btn, 1, row);
        add(newVault_btn, 2, row++);
        add(enabledBackup_lbl, 0, row);
        add(enableBackup_cbx, 1, row++);
        add(currentVaultLocation_lbl, 0, row);
        add(currentVaultLocation_txf, 1, row++, colspan * 2, rowspan);
        add(creationDate_lbl, 0, row);
        add(creationDate_txf, 1, row++);
        add(creationUser_lbl, 0, row);
        add(creationUser_txf, 1, row++);
        add(creationSystem_lbl, 0, row);
        add(creationSystem_txf, 1, row++);
        add(changeDate_lbl, 0, row);
        add(changeDate_txf, 1, row++);
        add(changeUser_lbl, 0, row);
        add(changeUser_txf, 1, row++);
        add(changeSystem_lbl, 0, row);
        add(changeSystem_txf, 1, row++);
    }

    public void actionLoadVault(boolean enableBackup) throws IOException {
        log.debug("actionLoadVault");
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(MC().RB().LOAD);
        File selectedFile = fileChooser.showOpenDialog(null);
        if (selectedFile != null) {
            MC().loadVault(selectedFile, enableBackup);
        }
        bindUi(selectedFile);
    }

    public void actionSaveVault() throws IOException {
        log.debug("actionSaveVault");
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(MC().RB().SAVE);
        File currentVaultFile = getCurrentVaultFileDirectory();
        if (currentVaultFile != null) {
            fileChooser.setInitialDirectory(currentVaultFile.getParentFile());
            fileChooser.setInitialFileName(currentVaultFileName());
        }
        File selectedFile = fileChooser.showSaveDialog(null);
        if (selectedFile != null) {
            MC().saveCurrentVault(selectedFile);
        }
        bindUi(selectedFile);
    }

    public void actionNewVault() throws IOException {
        log.debug("actionNewVault");
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(MC().RB().SAVE);
        File currentVaultFile = getCurrentVaultFileDirectory();
        if (currentVaultFile != null) {
            fileChooser.setInitialDirectory(currentVaultFile);
            fileChooser.setInitialFileName(currentVaultFileName());
        }
        File selectedFile = fileChooser.showSaveDialog(null);
        if (selectedFile != null) {
            MC().saveNewVault(selectedFile);
        }
        bindUi(selectedFile);
    }

    private File getCurrentVaultFileDirectory() {
        String dir = StringUtils.substringBeforeLast(currentVaultLocation_txf.getText(), "/");
        return StringUtils.isNotEmpty(dir) ? new File(dir) : null;
    }

    private String currentVaultFileName() {
        return StringUtils.substringAfterLast(currentVaultLocation_txf.getText(), "/");
    }

    @Override
    public void bindUi() {
        enableBackup_cbx.setSelected(MC().PREFS().getEnableBackup());
        bindUi(getCurrentVaultFile());
    }

    private void bindUi(File currentVaultFile) {
        if (currentVaultFile != null) {
            String currentVaultFilename = MC().getCurrentVault() != null ? currentVaultFile.getAbsolutePath() : "";
            currentVaultLocation_txf.setText(currentVaultFilename);
            creationDate_txf.setText(convertToDateAndTime(MC().getCurrentVault().getCreationDate()));
            creationUser_txf.setText(MC().getCurrentVault().getCreationUser());
            creationSystem_txf.setText(MC().getCurrentVault().getCreationSystem());
            changeDate_txf.setText(convertToDateAndTime(MC().getCurrentVault().getChangeDate()));
            changeUser_txf.setText(MC().getCurrentVault().getChangeUser());
            changeSystem_txf.setText(MC().getCurrentVault().getChangeSystem());
        }
    }

    private File getCurrentVaultFile() {
        if (StringUtils.isNotEmpty(currentVaultLocation_txf.getText())) {
            return new File(currentVaultLocation_txf.getText());
        } else {
            return null;
        }
    }

    @Override
    public String getTitle() {
        return MC().RB().VAULT;
    }

}
