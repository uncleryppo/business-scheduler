package de.ithappens.view.businessscheduler.model;

import java.util.Calendar;
import java.util.List;
import de.ithappens.businessscheduler.model.WorkingTimeContract;

/** 
 * <p>Title: org.y3.businessscheduler.view.model - Utils</p>
 * <p>Description: </p>
 * <p>Copyright: 2016</p>
 * <p>Organisation: IT-Happens.de</p>
 * @author Christian.Rybotycky
*/
public class Utils {
    
    public static WorkingTimeContract findValidContract(List<WorkingTimeContract> contracts, Calendar validationDate) {
        WorkingTimeContract validContract = null;
        if (contracts != null && !contracts.isEmpty() && validationDate != null) {
            for (WorkingTimeContract contract : contracts) {
                Calendar cvf = contract.getValidFrom();
                if (cvf != null && validationDate.compareTo(cvf) >= 0) {
                    if (validContract == null || validContract.getValidFrom().compareTo(cvf) >= 0) {
                        validContract = contract;
                    }
                }
            }
        }
        return validContract;
    }
    
}
