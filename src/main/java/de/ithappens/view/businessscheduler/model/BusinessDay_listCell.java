package de.ithappens.view.businessscheduler.model;

import java.text.DecimalFormat;
import de.ithappens.businessscheduler.model.BusinessDay;

/**
 * Copyright: 2016 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class BusinessDay_listCell extends IModel_listCell<BusinessDay> {

    @Override
    public String getItemText(BusinessDay item) {
        DecimalFormat df = new DecimalFormat("####0.00");
        return I_node.convertToDateAndTime(item.getDate()) + (" [" + df.format(item.getDuration()) + "]");
    }

    @Override
    public String getStyleClass(BusinessDay item) {
        if (item != null && item.isToday()) {
            return "calculatedTodayInfo";
        } else {
            return super.getStyleClass(item);
        }
    }

}
