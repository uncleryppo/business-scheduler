package de.ithappens.businessscheduler.model;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.controlsfx.control.Notifications;
import de.ithappens.businessscheduler.ResourceBundleDictionary;
import de.ithappens.businessscheduler.UserPreferences;
import de.ithappens.businessscheduler.view.IconDictionary;
import static de.ithappens.businessscheduler.view.IconDictionary.BASE_PATH;

/**
 * Copyright: 2016 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class ModelController {
	
	private Logger log = LogManager.getLogger();

    private final ResourceBundleDictionary RB = new ResourceBundleDictionary();
    private final String css = IconDictionary.class.getClassLoader().getResource(BASE_PATH + "business-scheduler.css").toExternalForm();
    private Vault currentVault;
    private File currentVaultFile;
    private final UserPreferences userPreferences = new UserPreferences();

    public ModelController() {
    }

    public ResourceBundleDictionary RB() {
        return RB;
    }

    public String CSS() {
        return css;
    }

    public UserPreferences PREFS() {
        return userPreferences;
    }

    public Vault getCurrentVault() {
        if (currentVault == null) {
            currentVault = new Vault();
            currentVault.setCreationDate(Calendar.getInstance());
            currentVault.setCreationUser(System.getProperty("user.name"));
            String hostname = null;
            try {
                java.net.InetAddress addr = java.net.InetAddress.getLocalHost();
                hostname = addr.getHostName();
            } catch (UnknownHostException ex) {
                log.error(ex);
            }
            currentVault.setCreationSystem(hostname);
        }
        return currentVault;
    }

    public Vault loadVault(String vaultLocation, boolean enableBackup) throws IOException {
        return loadVault(new File(vaultLocation), enableBackup);
    }

    public Vault loadVault(File vaultFile, boolean enableBackup) throws IOException {
        currentVaultFile = vaultFile;
        PREFS().setLocationOfLastUsedVaultFile(vaultFile.getPath());
        ObjectMapper objectMapper = new ObjectMapper();
        currentVault = objectMapper.readValue(vaultFile, Vault.class);
        if (enableBackup) {
            backupCurrenVault();
        }
        return currentVault;
    }

    public void backupCurrenVault() throws IOException {
        log.debug("backupCurrentVault");
        if (currentVaultFile != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            String backupFilename
                    = StringUtils.substringBeforeLast(currentVaultFile.getPath(), ".")
                    + PREFS().getSuffixForBackupFile()
                    + sdf.format(new Date())
                    + "."
                    + StringUtils.substringAfterLast(currentVaultFile.getPath(), ".");
            File backupVaultFile = new File(backupFilename);
            Files.copy(currentVaultFile.toPath(), backupVaultFile.toPath());
            String logInformation = RB.VAULT_BACKUP_SUCCESSFUL;
            Notifications
                    .create()
                    .title(RB.APPLICATION_NAME)
                    .text(logInformation)
                    .showInformation();
            log.debug(logInformation);
        } else {
            String logInformation = RB.VAULT_BACKUP_NOT_SUCCESSFUL;
            log.warn(logInformation);
            Notifications
                    .create()
                    .title(RB.APPLICATION_NAME)
                    .text(logInformation)
                    .showWarning();
        }
    }

    public void saveCurrentVault(String vaultLocation) throws IOException {
        saveCurrentVault(new File(vaultLocation));
    }

    public void saveCurrentVault() throws IOException {
        saveCurrentVault(currentVaultFile);
    }

    public void saveNewVault(File vaultFile) throws IOException {
        currentVault = null;
        saveCurrentVault(vaultFile);
    }

    public void saveCurrentVault(File vaultFile) throws IOException {
        currentVaultFile = vaultFile;
        if (currentVault == null) {
            getCurrentVault();
        }
        currentVault.setChangeDate(Calendar.getInstance());
        currentVault.setChangeUser(System.getProperty("user.name"));
        String hostname = null;
        try {
            java.net.InetAddress addr = java.net.InetAddress.getLocalHost();
            hostname = addr.getHostName();
        } catch (UnknownHostException ex) {
            log.error(ex);
        }
        currentVault.setChangeSystem(hostname);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(vaultFile, currentVault);
        PREFS().setLocationOfLastUsedVaultFile(vaultFile.getPath());
    }

}
