package de.ithappens.businessscheduler.model;

import java.util.Calendar;
import org.codehaus.jackson.annotate.JsonIgnore;

/** 
 * Title: WorkingTimeContract
 * Copyright: 2016 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
*/
public class WorkingTimeContract implements I_model<WorkingTimeContract> {
    
    private Calendar validFrom;
    public Calendar getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Calendar validFrom) {
		this.validFrom = validFrom;
	}

	private Integer 
            workingMinutesPerDay, 
            allowedWorkingMinutesPerDayWithoutBreak,
            breakMinutesPerDay,
            maxAllowedWorkingMinutesPerDay;

    public Integer getWorkingMinutesPerDay() {
		return workingMinutesPerDay;
	}

	public void setWorkingMinutesPerDay(Integer workingMinutesPerDay) {
		this.workingMinutesPerDay = workingMinutesPerDay;
	}

	public Integer getAllowedWorkingMinutesPerDayWithoutBreak() {
		return allowedWorkingMinutesPerDayWithoutBreak;
	}

	public void setAllowedWorkingMinutesPerDayWithoutBreak(Integer allowedWorkingMinutesPerDayWithoutBreak) {
		this.allowedWorkingMinutesPerDayWithoutBreak = allowedWorkingMinutesPerDayWithoutBreak;
	}

	public Integer getBreakMinutesPerDay() {
		return breakMinutesPerDay;
	}

	public void setBreakMinutesPerDay(Integer breakMinutesPerDay) {
		this.breakMinutesPerDay = breakMinutesPerDay;
	}

	public Integer getMaxAllowedWorkingMinutesPerDay() {
		return maxAllowedWorkingMinutesPerDay;
	}

	public void setMaxAllowedWorkingMinutesPerDay(Integer maxAllowedWorkingMinutesPerDay) {
		this.maxAllowedWorkingMinutesPerDay = maxAllowedWorkingMinutesPerDay;
	}

	@Override
    public int compareTo(WorkingTimeContract o) {
        if (o == null || o.getValidFrom() == null) {
            return -1;
        }
        if (getValidFrom() == null) {
            return 1;
        }
        return o.getValidFrom().compareTo(getValidFrom());
    }
    
    @JsonIgnore
    public Double getBreakHoursPerDay() {
        return convertMinutesToHours(breakMinutesPerDay);
    }
    
    @JsonIgnore
    public Double getWorkingHoursPerDay() {
        return convertMinutesToHours(workingMinutesPerDay);
    }
    
    @JsonIgnore
    public Double getWorkingHoursPerDayWithBreak() {
        return convertMinutesToHours(workingMinutesPerDay) + convertMinutesToHours(breakMinutesPerDay);
    }
    
    @JsonIgnore
    public Double getWorkingHoursPerDayWithBreakNetto() {
        return getWorkingHoursPerDayWithBreak() - convertMinutesToHours(breakMinutesPerDay);
    }
    
    @JsonIgnore
    public Double getDurationNetto(Double durationBrutto) {
        if (durationBrutto != null && durationBrutto > getAllowedWorkingHoursPerDayWithoutBreak()) {
            return durationBrutto - getBreakHoursPerDay();
        } else {
            return durationBrutto;
        }
    }
    
    @JsonIgnore
    public Double getAllowedWorkingHoursPerDayWithoutBreak() {
        return convertMinutesToHours(allowedWorkingMinutesPerDayWithoutBreak);
    }
    
    @JsonIgnore
    public Double getMaxAllowedWorkingHoursPerDay() {
        return convertMinutesToHours(maxAllowedWorkingMinutesPerDay);
    }
    
    @JsonIgnore
    public Double getMaxAllowedWorkingHoursPerDayNetto() {
        return getMaxAllowedWorkingHoursPerDay() - convertMinutesToHours(breakMinutesPerDay);
    }
    
    @JsonIgnore
    public Double getMaxAllowedWorkingHoursPerDayWithBreak() {
        return convertMinutesToHours(maxAllowedWorkingMinutesPerDay) + convertMinutesToHours(breakMinutesPerDay);
    }
    
    private Double convertMinutesToHours(Integer minutes) {
        return minutes != null 
                ? minutes.doubleValue() / 60
                : null;
    }
        
}
