package de.ithappens.businessscheduler.model;

import java.util.Calendar;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Copyright: 2016 - 2018
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class BusinessDay implements I_model<BusinessDay> {

    private Calendar startBusiness_timeStamp, endBusiness_timeStamp;
    private String notes_string;

    public String getNotes_string() {
        return notes_string;
    }

    public void setNotes_string(String notes_string) {
        this.notes_string = notes_string;
    }

    public Calendar getStartBusiness_timeStamp() {
        return startBusiness_timeStamp;
    }

    public void setStartBusiness_timeStamp(Calendar startBusiness_timeStamp) {
        this.startBusiness_timeStamp = startBusiness_timeStamp;
    }

    public Calendar getEndBusiness_timeStamp() {
        return endBusiness_timeStamp;
    }

    public void setEndBusiness_timeStamp(Calendar endBusiness_timeStamp) {
        this.endBusiness_timeStamp = endBusiness_timeStamp;
    }

    public Integer calculateCalendarWeek() {
        int calendarWeek = 0;
        Calendar sourceCalendarToCalculate = null;
        if (startBusiness_timeStamp != null) {
            sourceCalendarToCalculate = startBusiness_timeStamp;
        } else if (endBusiness_timeStamp != null) {
            sourceCalendarToCalculate = endBusiness_timeStamp;
        }
        if (sourceCalendarToCalculate != null) {
            calendarWeek = sourceCalendarToCalculate.get(Calendar.WEEK_OF_YEAR);
        }
        return calendarWeek;
    }

    @JsonIgnore
    public Calendar getDate() {
        return startBusiness_timeStamp != null ? startBusiness_timeStamp : endBusiness_timeStamp;
    }

    @JsonIgnore
    public double getDuration() {
        if (startBusiness_timeStamp == null || endBusiness_timeStamp == null) {
            return 0;
        } else {
            double hours = endBusiness_timeStamp.get(Calendar.HOUR_OF_DAY) - startBusiness_timeStamp.get(Calendar.HOUR_OF_DAY);
            double minutes = endBusiness_timeStamp.get(Calendar.MINUTE) - startBusiness_timeStamp.get(Calendar.MINUTE);
            double duration = hours + (minutes / 60);
            return duration;
        }
    }

    @JsonIgnore
    public boolean isToday() {
        boolean isToday = false;
        Calendar currentTimeStamp = Calendar.getInstance();
        if (getStartBusiness_timeStamp() != null
                && currentTimeStamp.get(Calendar.YEAR) == getStartBusiness_timeStamp().get(Calendar.YEAR)
                && currentTimeStamp.get(Calendar.MONTH) == getStartBusiness_timeStamp().get(Calendar.MONTH)
                && currentTimeStamp.get(Calendar.DAY_OF_MONTH) == getStartBusiness_timeStamp().get(Calendar.DAY_OF_MONTH)) {
            isToday = true;
        }
        return isToday;
    }

    @Override
    public int compareTo(BusinessDay o) {
        if (o == null || o.getDate() == null) {
            return -1;
        }
        if (getDate() == null) {
            return 1;
        }
        return o.getDate().compareTo(getDate());
    }

}
