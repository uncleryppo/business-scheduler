package de.ithappens.businessscheduler.model;

/** 
 * <p>Title: org.y3.businessscheduler.model - I_model</p>
 * <p>Description: </p>
 * <p>Copyright: 2016</p>
 * <p>Organisation: IT-Happens.de</p>
 * @author Christian.Rybotycky
*/
public interface I_model<E extends I_model> extends Comparable<E> {

}
