package de.ithappens.businessscheduler.model;

import java.util.Calendar;
import java.util.List;

/** 
 * Title: Vault
 * Description:
 * Copyright: 2016 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
*/
public class Vault {
    
    //meta
    private Calendar creationDate, changeDate;
    private String creationUser, creationSystem, changeUser, changeSystem;
    //content
    private List<BusinessDay> businessDays;
    private List<WorkingTimeContract> workingTimecontracts;
	public Calendar getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}
	public Calendar getChangeDate() {
		return changeDate;
	}
	public void setChangeDate(Calendar changeDate) {
		this.changeDate = changeDate;
	}
	public String getCreationUser() {
		return creationUser;
	}
	public void setCreationUser(String creationUser) {
		this.creationUser = creationUser;
	}
	public String getCreationSystem() {
		return creationSystem;
	}
	public void setCreationSystem(String creationSystem) {
		this.creationSystem = creationSystem;
	}
	public String getChangeUser() {
		return changeUser;
	}
	public void setChangeUser(String changeUser) {
		this.changeUser = changeUser;
	}
	public String getChangeSystem() {
		return changeSystem;
	}
	public void setChangeSystem(String changeSystem) {
		this.changeSystem = changeSystem;
	}
	public List<BusinessDay> getBusinessDays() {
		return businessDays;
	}
	public void setBusinessDays(List<BusinessDay> businessDays) {
		this.businessDays = businessDays;
	}
	public List<WorkingTimeContract> getWorkingTimecontracts() {
		return workingTimecontracts;
	}
	public void setWorkingTimecontracts(List<WorkingTimeContract> workingTimecontracts) {
		this.workingTimecontracts = workingTimecontracts;
	}

}
