package de.ithappens.businessscheduler;

import javafx.application.Application;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.ithappens.businessscheduler.model.ModelController;
import de.ithappens.businessscheduler.view.ApplicationFrame;
import de.ithappens.businessscheduler.view.IconDictionary;

/**
 * Copyright: 2016 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class BusinessScheduler extends Application {

    private static boolean undecorated = true;
    private static final String appId = "Business Scheduler";
    private static Logger log = LogManager.getLogger();

    private static String[] args;

    /**
     * @param _args the command line arguments
     */
    public static void main(String[] _args) {
        args = _args;
        if (isOSX()) {
            System.setProperty("apple.awt.graphics.EnableQ2DX", "true");
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", appId);
            System.setProperty("apple.laf.useScreenMenuBar", "true");
            //com.apple.eawt.Application.getApplication().setDockIconBadge(appId);
            com.apple.eawt.Application.getApplication().setDockIconImage(IconDictionary.APPLICATION__IMAGE_ICON.getImage());
        }
        readArgs();
        launch(args);
    }

    public static void readArgs() {
        if (args != null) {
            for (String arg : args) {
                undecorated = !StringUtils.contains(arg, "-decorated");
            }
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        log.debug("Starting modelController");
        ModelController mc = new ModelController();
        log.debug("Starting application " + mc.RB().APPLICATION_NAME + " in version " + mc.RB().APPLICATION_VERSION);
        ApplicationFrame applicationFrame = new ApplicationFrame(primaryStage, undecorated, mc);
        applicationFrame.setVisible(true);
        applicationFrame.loadLastUsedVaultFile();
    }

    public static boolean isOSX() {
        String osName = System.getProperty("os.name").toLowerCase();
        return osName.contains("mac");
    }

}
