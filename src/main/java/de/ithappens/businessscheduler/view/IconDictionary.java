package de.ithappens.businessscheduler.view;

import java.net.URL;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.swing.ImageIcon;

/**
 * Copyright: 2016 - 2018
 * Organisation: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class IconDictionary {

    public static final String BASE_PATH = "de/ithappens/";
    public static final String RESOURCE_PATH = BASE_PATH + "businessscheduler/";
    public static final String IMAGE_PATH = RESOURCE_PATH + "img/";

    public static final Image APPLICATION_ICON = getFxImage("155__coffe_tea.png");
    public static final Image FULLSCREEN_ICON = getFxImage("096__zoom in.png");
    public static final ImageIcon APPLICATION__IMAGE_ICON = getImageIcon("155__coffe_tea.png");
    public static final Image INFO_ICON = getFxImage("043__hint.png");
    public static final Image QUIT_ICON = getFxImage("057__power.png");

    private static final String TODAY = "073__focus.png";
    private static final String HOMEOFFICE = "101__home.png";
    private static final String HOUR_PLUS = "005__plus.png";
    private static final String HOUR_MINUS = "006__minus.png";
    private static final String MINUTE_PLUS = "015__circle_plus.png";
    private static final String MINUTE_MINUS = "016__circle_minus.png";
    public static final Image TODAY_ICON = getFxImage(TODAY);
    public static final ImageIcon TODAY_IMAGE_ICON = getImageIcon(TODAY);
    public static final ImageView TODAY_IMAGE_VIEW = getImageView(TODAY);
    public static final ImageView HOMEOFFICE_IMAGE_VIEW = getImageView(HOMEOFFICE);
    
    public static String MISSING_ICON = "picture-empty.png";

    public static final Image LOAD_ICON = getFxImage("025__login.png");
    public static final Image SAVE_ICON = getFxImage("026__logout.png");
    public static final Image NEW_VAULT_ICON = getFxImage("037__file_plus.png");
    public static ImageView HOUR_PLUS() {
        return getImageView(HOUR_PLUS);
    }
    public static ImageView HOUR_MINUS() {
        return getImageView(HOUR_MINUS);
    }
    public static ImageView MINUTE_PLUS() {
        return getImageView(MINUTE_PLUS);
    }
    public static ImageView MINUTE_MINUS() {
        return getImageView(MINUTE_MINUS);
    }
    
    public static Image getFxImage(String filename) {
        return new Image(IconDictionary.class.getClassLoader().getResourceAsStream(IMAGE_PATH + filename));
    }

    private static ImageIcon getImageIcon(String iconName) {
        URL resourceURL = IconDictionary.class.getClassLoader().getResource(IMAGE_PATH + iconName);
        if (resourceURL == null) {
            resourceURL = IconDictionary.class.getClassLoader().getResource(IMAGE_PATH + MISSING_ICON);
        }
        return new ImageIcon(resourceURL);
    }
    
    public static ImageView getImageView(String iconName) {
        return new ImageView(IMAGE_PATH + iconName);
    }

}
