package de.ithappens.businessscheduler.view;

import com.jfoenix.controls.JFXTabPane;
import de.ithappens.businessscheduler.model.ModelController;
import de.ithappens.view.businessscheduler.model.BusinessDay_node;
import de.ithappens.view.businessscheduler.model.I_node;
import de.ithappens.view.businessscheduler.model.Vault_node;
import de.ithappens.view.businessscheduler.model.WorkingTimeContract_node;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.animation.FadeTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/** 
 * Copyright: 2016 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
*/
public class ApplicationFrame {
	
	private Logger log = LogManager.getLogger();
    
    private ModelController MC;
    
    private final Stage primaryStage;
    private CheckMenuItem mi_fullScreen;
    private Menu menuApp;
    
    private MenuBar menuBar;
    private JFXTabPane content_tabPane;
    private Scene root_scene;
    
    private Vault_node vault_node;
    
    private double xOffset = 0;
    private double yOffset = 0;

    public ApplicationFrame(Stage _primaryStage, boolean undecorated, ModelController _modelController) {
        MC = _modelController;
        primaryStage = _primaryStage;
        buildUi(undecorated);
    }
    
    private void buildUi(boolean undecorated) {
        //common
        if (undecorated) {
            primaryStage.initStyle(StageStyle.UNDECORATED);
        } else {
            primaryStage.initStyle(StageStyle.DECORATED);
        }
        primaryStage.setTitle(MC.RB().APPLICATION_NAME + " " + MC.RB().APPLICATION_VERSION);
        primaryStage.getIcons().add(IconDictionary.APPLICATION_ICON);
        //menu
        buildMenu(undecorated);
        //layout
        content_tabPane = new JFXTabPane();
        content_tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                if (oldValue != null) {
                    FadeTransition fadeOldOutTransition = new FadeTransition(Duration.millis(500), oldValue.getContent());
                    fadeOldOutTransition.setFromValue(1.0);
                    fadeOldOutTransition.setToValue(0.0);
                    fadeOldOutTransition.playFromStart();
                }
                if (newValue != null) {
                    FadeTransition fadeNewInTransition = new FadeTransition(Duration.millis(500), newValue.getContent());
                    fadeNewInTransition.setFromValue(0.0);
                    fadeNewInTransition.setToValue(1.0);
                    fadeNewInTransition.playFromStart();
                    if (newValue.getContent() instanceof I_node) {
                        ((I_node) newValue.getContent()).bindUi();
                    }
                }
            }
        });
        BorderPane rootPane = new BorderPane(content_tabPane);
        rootPane.getStyleClass().add("rootPane");
        rootPane.setTop(menuBar);
        menuBar.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        menuBar.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        menuBar.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
        
        //content
        addTab(vault_node = new Vault_node(MC));
        addTab(new BusinessDay_node(MC));
        addTab(new WorkingTimeContract_node(MC));
        
        Group rootGroup = new Group();
        rootGroup.getChildren().add(rootPane);
        root_scene = new Scene(rootGroup);
        root_scene.getStylesheets().add(MC.CSS());
        primaryStage.setScene(root_scene);
        primaryStage.centerOnScreen();
    }
    
    private void addTab(I_node content) {
        Tab tab = new Tab(content.getTitle());
        tab.setContent(content);
        tab.setClosable(false);
        content_tabPane.getTabs().add(tab);
    }
    
    public void loadLastUsedVaultFile() {
        try {
            vault_node.loadLastUsedVaultFile(MC.PREFS().getEnableBackup());
        } catch (IOException ex) {
            log.error(ex);
        }
        
    }

    private void buildMenu(boolean undecorated) {
        //menu: application
        menuApp = new Menu(getAppMenuName(), getAppMenuIcon());
        menuApp.setOnShowing((Event event) -> {
            mi_fullScreen.setSelected(primaryStage.isFullScreen());
        });
        mi_fullScreen = new CheckMenuItem(MC.RB().FULLSCREEN);
        mi_fullScreen.setGraphic(new ImageView(IconDictionary.FULLSCREEN_ICON));
        mi_fullScreen.setOnAction((ActionEvent t) -> {
            actionSwitchFullScreen();
        });
        MenuItem mi_about = new MenuItem(MC.RB().ABOUT);
        mi_about.setGraphic(new ImageView(IconDictionary.INFO_ICON));
        mi_about.setOnAction((ActionEvent t) -> {
            actionShowAboutDialog();
        });
        MenuItem mi_quit = new MenuItem(MC.RB().QUIT);
        mi_quit.setGraphic(new ImageView(IconDictionary.QUIT_ICON));
        mi_quit.setOnAction(((ActionEvent t) -> {
            log.debug("Shut down application by menu item.");
            System.exit(0);
        }));
        menuApp.getItems().addAll(mi_about, mi_fullScreen, new SeparatorMenuItem(), mi_quit);
        //menu bar
        menuBar = new MenuBar();
        menuBar.getMenus().addAll(menuApp);
    }
    
    public void actionShowAboutDialog() {
        log.debug("Show about dialog.");
        AboutDialog aboutDialog = new AboutDialog(primaryStage.getOwner(), MC);
        aboutDialog.showAndWait();
    }

    public void actionSwitchFullScreen() {
        primaryStage.setFullScreen(!primaryStage.isFullScreen());
        menuApp.setText(getAppMenuName());
        menuApp.setGraphic(getAppMenuIcon());
    }
    
    public String getAppMenuName() {
        return primaryStage.isFullScreen() ? MC.RB().APPLICATION_NAME : MC.RB().APPLICATION;
    }
    
    public ImageView getAppMenuIcon() {
        return primaryStage.isFullScreen() ? new ImageView(IconDictionary.APPLICATION_ICON) : null;
    }

    public void setVisible(boolean visible) {
        if (visible) {
            primaryStage.show();
        } else {
            primaryStage.hide();
        }
    }

}
