package de.ithappens.businessscheduler.view;

import de.ithappens.commons.usercontext.UserContext;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import de.ithappens.businessscheduler.model.ModelController;

/** 
 * Copyright: 2016 - 2017
 * <p>Organisation: IT-Happens.de</p>
 * @author Christian.Rybotycky
*/
public class AboutDialog extends Dialog<Object> {

    private final ModelController MC;
    private GridPane grid;
    private int gridRows = 0;
    private final UserContext USERCONTEXT = UserContext.getInstance();

    public AboutDialog(Window owner, ModelController _modelController) {
        super();
        MC = _modelController;
        initOwner(owner);
        buildUi();
    }
    
    private void buildUi() {
        setResizable(false);
        initStyle(StageStyle.UTILITY);
        getDialogPane().getStylesheets().add(MC.CSS());
        ((Stage) getDialogPane().getScene().getWindow()).getIcons().add(IconDictionary.INFO_ICON);
        
        String applicationName = MC.RB().APPLICATION_NAME;
        String applicationVersion = MC.RB().APPLICATION_VERSION;
        String[] developers = new String[]{"Christian Rybotycky"};
        
        setTitle(MC.RB().ABOUT + " " + applicationName + " " + applicationVersion);
        setHeaderText(applicationName + "\n" + MC.RB().VERSION + " " + applicationVersion + " - " + MC.RB().FILE_VERSION);
        setGraphic(new ImageView(IconDictionary.APPLICATION_ICON));
        
        addGridRow(MC.RB().PROVIDED_BY, "IT-Happens.de");
        if (developers != null && developers.length > 0) {
            String developedByLabel = MC.RB().DEVELOPED_BY;
            addGridRow(developedByLabel, developers[0]);
            for (int dNo = 1; dNo < developers.length; dNo++) {
                addGridRow("", developers[dNo]);
            }
        }
        addGridSeparator();
        addGridRow(MC.RB().OS_ARCHITECTURE, USERCONTEXT.getOperatingSystemEnvironment().getSystemarchitecture());
        addGridRow(MC.RB().OS_NAME, USERCONTEXT.getOperatingSystemEnvironment().getSystemname());
        addGridRow(MC.RB().OS_VERSION, USERCONTEXT.getOperatingSystemEnvironment().getSystemversion());
        addGridRow(MC.RB().OS_USER_DIR, USERCONTEXT.getOperatingSystemEnvironment().getUserDir());
        addGridRow(MC.RB().OS_USER_HOME, USERCONTEXT.getOperatingSystemEnvironment().getUserHome());
        addGridSeparator();
        addGridRow(MC.RB().JAVA_VERSION, USERCONTEXT.getJavaEnvironment().getJavaVersion());
        addGridRow(MC.RB().JAVA_LOCATION, USERCONTEXT.getJavaEnvironment().getJavaLocation());
        addGridRow(MC.RB().JAVA_VENDOR, USERCONTEXT.getJavaEnvironment().getJavaVendor());
        addGridRow(MC.RB().JAVA_VENDOR_URL, USERCONTEXT.getJavaEnvironment().getJavaVendorUrl());
        
        ButtonType buttonTypeOk = new ButtonType(MC.RB().OK, ButtonData.OK_DONE);
        getDialogPane().getButtonTypes().add(buttonTypeOk);
        setResultConverter((ButtonType param) -> null);
        
    }
    
    private void addGridSeparator() {
        provideGrid();
        grid.add(new Separator(), 1, gridRows, 2, 1);
        gridRows++;
    }
    
    private void provideGrid() {
        if (grid == null) {
            grid = new GridPane();
            grid.setAlignment(Pos.CENTER);
            getDialogPane().setContent(grid);
            grid.getStyleClass().add("grid");
        }
    }
    
    private void addGridRow(String title, String value) {
        provideGrid();
        Label l_title = new Label(title);
        Label l_value = new Label(value);
        l_value.getStyleClass().add("titleLabel");
        grid.add(l_title, 1, gridRows);
        grid.add(l_value, 2, gridRows);
        gridRows++;
    }

}
