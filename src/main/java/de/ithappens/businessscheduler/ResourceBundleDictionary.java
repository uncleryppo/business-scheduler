package de.ithappens.businessscheduler;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;

/** 
 * Copyright: 2016 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
*/
public class ResourceBundleDictionary {
    public final ResourceBundle RB = ResourceBundle.getBundle("de.ithappens.business-scheduler");
    //application
    public String ABOUT = get("ABOUT");
    public String APPLICATION = get("APPLICATION");
    public String APPLICATION_NAME = get("APPLICATION_NAME");
    public String APPLICATION_VERSION = get("APPLICATION_VERSION");
    public String DEVELOPED_BY = get("DEVELOPED_BY");
    public String FILE_VERSION = get("FILE_VERSION");
    public String FULLSCREEN = get("FULLSCREEN");
    public String OK = get("OK");
    public String PROVIDED_BY = get("PROVIDED_BY");
    public String VERSION = get("VERSION");
    public String QUIT = get("QUIT");
    public String OS_ARCHITECTURE = get("OS_ARCHITECTURE");
    public String OS_NAME = get("OS_NAME");
    public String OS_VERSION = get("OS_VERSION");
    public String OS_USER_DIR = get("OS_USER_DIR");
    public String OS_USER_HOME = get("OS_USER_HOME");
    public String JAVA_VERSION = get("JAVA_VERSION");
    public String JAVA_LOCATION = get("JAVA_LOCATION");
    public String JAVA_VENDOR = get("JAVA_VENDOR");
    public String JAVA_VENDOR_URL = get("JAVA_VENDOR_URL");
    //functions
    public String LOAD = get("LOAD");
    public String SAVE = get("SAVE");
    public String SAVE_DONE = get("SAVE_DONE");
    public String UPDATE = get("UPDATE");
    public String ADD = get("ADD");
    public String DELETE = get("DELETE");
    //storage: vault
    public String VAULT_LOCATION = get("VAULT_LOCATION");
    public String VAULT = get("VAULT");
    public String NEW_VAULT = get("NEW_VAULT");
    public String CREATION_DATE = get("CREATION_DATE");
    public String CREATION_USER = get("CREATION_USER");
    public String CREATION_SYSTEM = get("CREATION_SYSTEM");
    public String CHANGE_DATE = get("CHANGE_DATE");
    public String CHANGE_USER = get("CHANGE_USER");
    public String CHANGE_SYSTEM = get("CHANGE_SYSTEM");
    public String VAULT_BACKUP_SUCCESSFUL = get("VAULT_BACKUP_SUCCESSFUL");
    public String VAULT_BACKUP_NOT_SUCCESSFUL = get("VAULT_BACKUP_NOT_SUCCESSFUL");
    public String ENABLE_BACKUP = get("ENABLE_BACKUP");
    //model: business day
    public String BUSINESS_DAY = get("BUSINESS_DAY");
    public String BUSINESS_DAYS = get("BUSINESS_DAYS");
    public String ADD_BUSINESS_DAY = get("ADD_BUSINESS_DAY");
    public String DELETE_BUSINESS_DAY = get("DELETE_BUSINESS_DAY");
    public String START = get("START");
    public String END = get("END");
    public String NOTES = get("NOTES");
    public String CW = get("CW");
    public String TODAY = get("TODAY");
    public String ENABLE_SCROLL_TO_SELECTED_BUSINESS_DAY = get("ENABLE_SCROLL_TO_SELECTED_BUSINESS_DAY");
    public String ENABLE_TIMESTAMP_ANIMATION = get("ENABLE_TIMESTAMP_ANIMATION");
    public String HOME_OFFICE = get("HOME_OFFICE");
    public String HOUR_PLUS = get("HOUR_PLUS");
    public String HOUR_MINUS = get("HOUR_MINUS");
    public String MINUTE_PLUS = get("MINUTE_PLUS");
    public String MINUTE_MINUS = get("MINUTE_MINUS");
    //status
    public String HOURS = get("HOURS");
    public String BRUTTO = get("BRUTTO");
    public String NETTO = get("NETTO");
    public String COMPLETED = get("COMPLETED");
    //model: working time contract
    public String WORKING_TIME_CONTRACT = get("WORKING_TIME_CONTRACT");
    public String WORKING_TIME_CONTRACTS = get("WORKING_TIME_CONTRACTS");
    public String ALLOWED_WORKING_MINUTES_PER_DAY_WITHOUT_BREAK = get("ALLOWED_WORKING_MINUTES_PER_DAY_WITHOUT_BREAK");
    public String BREAK_MINUTES_PER_DAY = get("BREAK_MINUTES_PER_DAY");
    public String VALID_FROM = get("VALID_FROM");
    public String WORKING_MINUTES_PER_DAY = get("WORKING_MINUTES_PER_DAY");
    public String MAX_ALLOWED_WORKING_MINUTES_PER_DAY = get("MAX_ALLOWED_WORKING_MINUTES_PER_DAY");

    public String get(String key) {
        return get(RB, key);
    }

    public String get(ResourceBundle bundle, String key) {
        String value = null;
        try {
            value = bundle.getString(key);
        } catch (MissingResourceException mre) {
            LogManager.getLogger().warn("Requested resource '" + key + "' missing in bundle of locale '" + bundle.getLocale().toString() + "'.", mre);
        }
        return value != null ? value : key;
    }

}
