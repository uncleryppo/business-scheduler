package de.ithappens.businessscheduler.view.model;

import de.ithappens.view.businessscheduler.model.Utils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import de.ithappens.businessscheduler.model.WorkingTimeContract;

/**
 * Copyright: 2016 - 2019
 * Organization: IT-Happens.de
 * @author Christian.Rybotycky
 */
public class UtilsIT {
	
	private Logger log = LogManager.getLogger();
    
    @Rule
    public TestName name = new TestName();
    
    public UtilsIT() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findValidContract method, of class Utils.
     */
    @Test
    public void testFindValidContract_noContracts_noValidationDate() {
        log.debug(name.getMethodName());
        List<WorkingTimeContract> contracts = null;
        Calendar validationDate = null;
        WorkingTimeContract result = Utils.findValidContract(contracts, validationDate);
        Assert.assertNull(result);
    }
    
    /**
     * Test of findValidContract method, of class Utils.
     */
    @Test
    public void testFindValidContract_1Contract_noValidationDate() {
        log.debug(name.getMethodName());
        WorkingTimeContract contract = new WorkingTimeContract();
        contract.setValidFrom(Calendar.getInstance());
        List<WorkingTimeContract> contracts = new ArrayList<>();
        contracts.add(contract);
        Calendar validationDate = null;
        WorkingTimeContract result = Utils.findValidContract(contracts, validationDate);
        Assert.assertNull(result);
    }
    
    /**
     * Test of findValidContract method, of class Utils.
     */
    @Test
    public void testFindValidContract_1Contract_1ValidationDate_valid() {
        log.debug(name.getMethodName());
        WorkingTimeContract contract = new WorkingTimeContract();
        contract.setValidFrom(Calendar.getInstance());
        List<WorkingTimeContract> contracts = new ArrayList<>();
        contracts.add(contract);
        Calendar validationDate = Calendar.getInstance();
        WorkingTimeContract result = Utils.findValidContract(contracts, validationDate);
        Assert.assertEquals(contract, result);
    }
    
}
